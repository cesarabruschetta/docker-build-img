FROM phusion/baseimage:0.10.0

ADD ./ /code
WORKDIR /code

RUN ./scripts/install_libs.sh
RUN ./scripts/install_configs.sh
RUN rm -r /code 

WORKDIR /opt
CMD ["/sbin/my_init -- bash -l"]